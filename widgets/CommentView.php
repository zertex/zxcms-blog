<?php

namespace common\modules\blog\widgets;

use common\modules\blog\entities\BlogComment;

class CommentView
{
    public $comment;
    /**
     * @var self[]
     */
    public $children;

    public function __construct(BlogComment $comment, array $children)
    {
        $this->comment = $comment;
        $this->children = $children;
    }
}
