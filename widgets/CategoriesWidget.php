<?php

namespace common\modules\blog\widgets;

use common\modules\blog\entities\BlogCategory;
use common\modules\blog\repositories\read\BlogCategoryReadRepository;
use yii\base\Widget;

class CategoriesWidget extends Widget
{
    /** @var BlogCategory|null */
    public $active;

    private $_categories;

    public function __construct(BlogCategoryReadRepository $categories, $config = [])
    {
        parent::__construct($config);
        $this->_categories = $categories;
    }

    public function run(): string
    {
        return $this->render('categories/categories', [
            'categories' => $this->_categories->getAll(),
        ]);
    }
}
