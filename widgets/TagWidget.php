<?php
/**
 * Created by Error202
 * Date: 30.01.2018
 */

namespace common\modules\blog\widgets;

use common\modules\blog\entities\BlogPost;
use yii\base\Widget;

class TagWidget extends Widget
{
    public $post_id;

    public function run()
    {

        // tags from categories with assignments, ordered by count
        $tags = BlogPost::find()->select('t.name as name, count(t.id) as cnt, t.id as id')
                        ->from('blog_posts as p, blog_tag_assignments as a, blog_tags as t')
                        ->andWhere('p.id = a.post_id')
                        ->andWhere('t.id = a.tag_id')
                        ->andWhere(isset($this->post_id) ? 'p.id = ' . $this->post_id : '')
                        ->groupBy('t.id')
                        ->orderBy('cnt DESC')
                        ->limit(20)
                        ->asArray()
                        ->all();

        if ($tags) {
            return $this->render('tags/tags', [
                'tags' => $tags,
            ]);
        } else {
            return '';
        }
    }
}
