<?php
/**
 * Created by Error202
 * Date: 24.01.2018
 */

namespace common\modules\blog\widgets;

use common\modules\blog\entities\BlogTagAssignment;
use common\modules\blog\repositories\read\BlogPostReadRepository;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class PostByTagsWidget extends Widget
{
    public $count;
    public $view;
    public $post;

    public $posts;

    public function __construct(BlogPostReadRepository $posts, $config = [])
    {
        parent::__construct($config);
        $this->count = $this->count ? $this->count : 5;
        $this->posts = $posts;
    }

    public function run(): string
    {
        $tag_ids = ArrayHelper::getColumn(BlogTagAssignment::find()->andWhere(['post_id' => $this->post->id])->all(), 'tag_id');
        $posts   = $this->posts->getByTagsId($this->post, $tag_ids, $this->count)->getModels();

        return $this->render($this->view, [
            'posts' => $posts,
            'view'  => $this->view,
            'url'   => Url::canonical(),
        ]);
    }
}
