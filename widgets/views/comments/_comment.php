<?php

/* @var $item \common\modules\blog\widgets\CommentView */
/* @var $child integer */
?>

<div data-id="<?= $item->comment->id ?>" class="blog-comment <?= $child ? 'blog-comment-child' : 'blog-comment-parent' ?>" style="margin-left: <?= $child * 30 ?>px">

	<div class="media mb-4">
		<img class="d-flex mr-3 rounded-circle blog-comment-avatar" src="<?= Yii::$app->avatar->show($item->comment->user->username) ?>" alt="">
		<div class="media-body">
			<a name="comment_<?= $item->comment->id ?>"></a>
			<h5 class="mt-0"><?= $item->comment->user->username ?>
			<time class="blog-comment-time" datetime="<?= date('Y', $item->comment->created_at) ?>">
				<?php
				$date = ($item->comment->created_at > time() - 60*60*24*365*50
					? Yii::$app->formatter->asRelativeTime($item->comment->created_at)
					: Yii::$app->formatter->asDatetime($item->comment->created_at));
				?>
				<?= $date ?>
			</time>
			</h5>
			<p>
			<?php if ($item->comment->isActive()): ?>
				<?= Yii::$app->formatter->asNtext($item->comment->text) ?>
			<?php else: ?>
				<i>Comment is deleted.</i>
			<?php endif; ?>
			</p>
			<div class="blog-comment-footer">
				<?php if (!Yii::$app->user->isGuest): ?>
					<div class="blog-comment-reply"><a href="#"><?= Yii::t('post', 'Reply') ?></a></div>
				<?php endif; ?>
			</div>
			<div class="reply-block"></div>
		</div>
	</div>


	<!--
	<div class="post-comment-main">
		<div class="post-comment-header">
			<a name="comment_<?= $item->comment->id ?>"></a>
			<h5 class="author-name"><?= $item->comment->user->username ?></h5>
			<time class="post-comment-time" datetime="<?= date('Y', $item->comment->created_at) ?>">
				<?php
				$date = ($item->comment->created_at > time() - 60*60*24*365*50
					? Yii::$app->formatter->asRelativeTime($item->comment->created_at)
					: Yii::$app->formatter->asDatetime($item->comment->created_at));
				?>
				<?= $date ?>
			</time>
		</div>
		<div class="post-comment-text">
			<p>
				<?php if ($item->comment->isActive()): ?>
					<?= Yii::$app->formatter->asNtext($item->comment->text) ?>
				<?php else: ?>
					<i>Comment is deleted.</i>
				<?php endif; ?>
			</p>
		</div>
		<div class="post-comment-footer">
			<?php if (!Yii::$app->user->isGuest): ?>
				<div class="comment-reply"><span class="icon fa fa-comment"></span><a href="#"><?= Yii::t('post', 'Reply') ?></a></div>
			<?php endif; ?>
		</div>

		<div class="reply-block"></div>

	</div>-->
</div>



<?php foreach ($item->children as $children): ?>
	<?= $this->render('_comment', ['item' => $children, 'child' => $child + 1]) ?>
<?php endforeach; ?>
