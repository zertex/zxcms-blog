<?php
/**
 * Created by Error202
 * Date: 21.06.2018
 */

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $tags array
 */

?>

<hr>
<ul class="list-unstyled">
<?php foreach ($tags as $tag): ?>
	<li class="list-inline-item"><i class="fa fa-tag" aria-hidden="true"></i> <?= Html::a($tag['name'], ['/blog/post/tag', 'id' => $tag['id']]) ?></li>
<?php endforeach; ?>
</ul>