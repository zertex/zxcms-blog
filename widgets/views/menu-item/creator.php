<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

/**
 * @var $this \yii\web\View
 * @var $model \core\forms\menu\MenuItemForm
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\helpers\Url;

$block_name = Yii::t('blog', 'Blog');
$block_title_attr = Yii::t('blog', 'Blog');

$js = <<<JS
	function selectBlogPostOn() {
    	$("#post_select").removeAttr('disabled');
    	updateBlogUrl('post');
	}

	function selectBlogPostOff() {
    	$("#post_select").attr('disabled', 'disabled');
    	updateBlogUrl('blog');
	}
	
	function updateBlogUrl(type) {
    	if (type === 'blog') {
    	    $("#blog_menu_item_url").val('/blog/post/index');
    	    $("#blog_menu_item_url_params").val('');
    	    $("#blog_menu_item_name").val('{$block_name}');
    	    $("#blog_menu_item_title_attr").val('$block_title_attr');
    	}
    	else {
    	    var select = $("#post_select");
    	    var data = select.select2('data');
    	    var selected_post = select.val();
    	    $("#blog_menu_item_url").val('/blog/post/post/');
    	    $("#blog_menu_item_url_params").val('{"id":'+selected_post+'}');
    	    $("#blog_menu_item_name").val(data[0].text);
    	    $("#blog_menu_item_title_attr").val(data[0].text);
    	}
	}
JS;
$this->registerJs($js, $this::POS_HEAD);

$fetchUrl = Url::to( [ '/blog/manage/post/post-search' ] );
?>

<div class="menu_item_widget">

	<div class="form-group">
		<div class="radio">
			<?= Html::label(Html::radio('blog_item_type', true, [
				'value' => 'blog_home',
				'onclick' => new JsExpression('selectBlogPostOff()'),
			]) . Yii::t('blog', 'Blog Home')) ?>
		</div>

		<div class="radio">
			<?= Html::label(Html::radio('blog_item_type', false, [
					'value' => 'blog_post',
					'onclick' => new JsExpression('selectBlogPostOn()'),
				]) . Yii::t('blog', 'Blog Post')) ?>
		</div>

		<div>
			<?= \kartik\widgets\Select2::widget([
				'name' => 'post_select',
			    'value' => '',
			    'options' => [
			    	'placeholder' => Yii::t('blog', 'Select post...'),
				    'id' => 'post_select',
				    'onchange' => new JsExpression("updateBlogUrl('post')"),
			    ],
				'pluginOptions' => [
					'disabled' => true,
					'ajax' => [
						'url' => $fetchUrl,
						'dataType' => 'json',
						'data' => new JsExpression('function(params) { return {q:params.term}; }')
					],
					'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
					'templateResult' => new JsExpression('function(tag) { return tag.text; }'),
					'templateSelection' => new JsExpression('function (tag) { return tag.text; }'),
				],
			]) ?>
		</div>
	</div>

<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->hiddenInput([
		'id' => 'blog_menu_item_name',
	])->label(false) ?>

	<?= $form->field($model, 'title_attr')->hiddenInput([
		'id' => 'blog_menu_item_title_attr',
	])->label(false) ?>

	<?= $form->field($model, 'module')->hiddenInput([
	])->label(false) ?>

	<?= $form->field($model, 'menu_id')->hiddenInput([
	])->label(false) ?>

	<?= $form->field($model, 'url')->hiddenInput([
		'id' => 'blog_menu_item_url',
	])->label(false) ?>

	<?= $form->field($model, 'url_params')->hiddenInput([
		'value' => '',
		'id' => 'blog_menu_item_url_params',
	])->label(false) ?>

<div class="form-group">
<?= Html::submitButton(Yii::t('buttons', 'Add to menu'), [
	'class' => 'btn btn-info btn-sm pull-right'
]) ?>
</div>

<?php ActiveForm::end(); ?>

</div>