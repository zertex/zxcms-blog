<?php
/**
 * Created by Error202
 * Date: 10.07.2018
 */

namespace common\modules\blog\widgets;

use core\forms\menu\MenuItemForm;
use yii\base\Widget;

class MenuItemCreatorWidget extends Widget
{
    public $menu_id;

    public function run()
    {
        $form             = new MenuItemForm();
        $form->module     = 'blog';
        $form->name       = \Yii::t('blog', 'Blog');
        $form->title_attr = \Yii::t('blog', 'Blog');
        $form->menu_id    = $this->menu_id;
        $form->url        = '/blog/post/index';

        return $this->render('menu-item/creator', [
            'model' => $form,
        ]);
    }
}
