<?php

namespace common\modules\blog\repositories\read;

use common\modules\blog\entities\BlogCategory;
use common\modules\blog\entities\BlogPost;
use common\modules\blog\entities\BlogTag;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\db\ActiveQuery;

class BlogPostReadRepository
{
    public function count(): int
    {
        return BlogPost::find()->active()->pubDate()->typePublic()->count();
    }

    public function getAllByRange($offset, $limit): array
    {
        return BlogPost::find()->active()->pubDate()->typePublic()->orderBy(['id' => SORT_ASC])->limit($limit)->offset($offset)->all();
    }

    public function getAll(): DataProviderInterface
    {
        $query = BlogPost::find()->active()->pubDate()->typePublic()->with('category');

        return $this->getProvider($query);
    }

    public function getAllByCategory(BlogCategory $category): DataProviderInterface
    {
        $query = BlogPost::find()->active()->pubDate()->typePublic()->andWhere(['category_id' => $category->id])->with('category');

        return $this->getProvider($query);
    }

    public function findNext(int $id): ?BlogPost
    {
        return BlogPost::find()->active()->pubDate()->typePublic()->andWhere(['>', 'id', $id])->one();
    }

    public function findPrev(int $id): ?BlogPost
    {
        return BlogPost::find()->active()->pubDate()->typePublic()->andWhere([
            '<',
            'id',
            $id
        ])->orderBy(['id' => SORT_DESC])->one();
    }


    public function getAllByTag(BlogTag $tag): DataProviderInterface
    {
        $query = BlogPost::find()->alias('p')->active('p')->typePublic('p')->with('category');
        $query->joinWith(['blogTagAssignments ta'], false);
        $query->andWhere(['ta.tag_id' => $tag->id]);
        $query->groupBy('p.id');

        return $this->getProvider($query);
    }

    public function getByTagsId(BlogPost $post, array $tag_ids, int $limit = 15): DataProviderInterface
    {
        $query = BlogPost::find()->alias('p')->active('p')->typePublic('p')->with('category');
        $query->joinWith(['blogTagAssignments ta'], false);
        $query->andWhere(['ta.tag_id' => $tag_ids]);
        $query->andWhere(['!=', 'p.id', $post->id]);
        $query->groupBy('p.id');
        $query->limit($limit);

        return $this->getProvider($query);
    }

    public function getLast($limit): array
    {
        return BlogPost::find()->with('category')->orderBy(['id' => SORT_DESC])->limit($limit)->all();
    }

    public function getPopular($limit): array
    {
        return BlogPost::find()->with('category')->orderBy(['comments_count' => SORT_DESC])->limit($limit)->all();
    }

    public function find($id): ?BlogPost
    {
        return BlogPost::find()->active()->andWhere(['id' => $id])->one();
    }

    private function getProvider(ActiveQuery $query): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);
    }

    public function findBySlug($slug): ?BlogPost
    {
        return BlogPost::find()->andWhere(['slug' => $slug, 'type' => BlogPost::TYPE_PUBLIC])->one();
    }

    public function findPreview($id, $language): ?BlogPost
    {
        return BlogPost::find()->localized($language)->andWhere(['id' => $id])->one();
    }
}
