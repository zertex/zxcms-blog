<?php

namespace common\modules\blog\repositories\read;

use common\modules\blog\entities\BlogCategory;

class BlogCategoryReadRepository
{
    public function getAll(): array
    {
        return BlogCategory::find()->orderBy('sort')->all();
    }

    public function find($id): ?BlogCategory
    {
        return BlogCategory::findOne($id);
    }

    public function findBySlug($slug): ?BlogCategory
    {
        return BlogCategory::find()->andWhere(['slug' => $slug])->one();
    }
}
