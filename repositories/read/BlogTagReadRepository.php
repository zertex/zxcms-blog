<?php

namespace common\modules\blog\repositories\read;

use common\modules\blog\entities\BlogTag;

class BlogTagReadRepository
{
    public function find($id): ?BlogTag
    {
        return BlogTag::findOne($id);
    }

    public function findBySlug($slug): ?BlogTag
    {
        return BlogTag::find()->andWhere(['slug' => $slug])->one();
    }
}
