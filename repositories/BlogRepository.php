<?php

namespace common\modules\blog\repositories;

use common\modules\blog\entities\BlogPost;
use core\repositories\NotFoundException;

class BlogRepository
{
    public function get($id): BlogPost
    {
        if (!$post = BlogPost::findOne($id)) {
            throw new NotFoundException('Post is not found.');
        }
        return $post;
    }

    public function existsByCategory($id): bool
    {
        return BlogPost::find()->andWhere(['category_id' => $id])->exists();
    }

    public function save(BlogPost $post): void
    {
        if (!$post->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(BlogPost $post): void
    {
        if (!$post->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
