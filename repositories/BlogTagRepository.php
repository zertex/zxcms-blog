<?php

namespace common\modules\blog\repositories;

use common\modules\blog\entities\BlogTag;
use core\repositories\NotFoundException;

class BlogTagRepository
{
    public function get($id): BlogTag
    {
        if (!$tag = BlogTag::findOne($id)) {
            throw new NotFoundException('Tag is not found.');
        }
        return $tag;
    }

    public function findByName($name): ?BlogTag
    {
        return BlogTag::findOne(['name' => $name]);
    }

    public function save(BlogTag $tag): void
    {
        if (!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(BlogTag $tag): void
    {
        if (!$tag->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
