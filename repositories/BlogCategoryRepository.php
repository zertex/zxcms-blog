<?php

namespace common\modules\blog\repositories;

use common\modules\blog\entities\BlogCategory;
use core\repositories\NotFoundException;

class BlogCategoryRepository
{
    public function get($id): BlogCategory
    {
        if (!$category = BlogCategory::findOne($id)) {
            throw new NotFoundException('Category is not found.');
        }
        return $category;
    }

    public function save(BlogCategory $category): void
    {
        if (!$category->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(BlogCategory $category): void
    {
        if (!$category->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}
