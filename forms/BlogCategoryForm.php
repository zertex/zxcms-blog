<?php

namespace common\modules\blog\forms;

use common\modules\blog\entities\BlogCategory;
use core\components\LanguageDynamicModel;
use core\validators\SlugValidator;
use Yii;

class BlogCategoryForm extends LanguageDynamicModel
{
    public $name;
    public $slug;
    public $title;
    public $description;
    public $sort;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public $_category;

    public function __construct(BlogCategory $category = null, array $attributes = [], $config = [])
    {
        if ($category) {
            $this->slug      = $category->slug;
            $this->sort      = $category->sort;
            $this->_category = $category;
        } else {
            $this->sort = BlogCategory::find()->max('sort') + 1;
        }
        parent::__construct($category, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['name'], 'required'],
                [['name', 'slug', 'title', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['description', 'meta_description'], 'string'],
                ['slug', SlugValidator::class],
                [
                    ['slug'],
                    'unique',
                    'targetClass' => BlogCategory::class,
                    'filter'      => $this->_category ? ['<>', 'id', $this->_category->id] : null
                ]
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'name'             => Yii::t('blog', 'Name'),
                'slug'             => Yii::t('blog', 'SEO link'),
                'sort'             => Yii::t('blog', 'Sort'),
                'title'            => Yii::t('blog', 'Title'),
                'description'      => Yii::t('blog', 'Description'),
                'meta_title'       => Yii::t('blog', 'META Title'),
                'meta_description' => Yii::t('blog', 'META Description'),
                'meta_keywords'    => Yii::t('blog', 'META Keywords'),
            ]
        );
    }

    public function attributeHints()
    {
        return array_merge(
            parent::attributeHints(),
            [
                'slug' => Yii::t('blog', 'SEO link will be generated automatically if not specified'),
            ]
        );
    }

    public function updateSort()
    {
        $this->sort = BlogCategory::find()->max('sort') + 1;
    }
}
