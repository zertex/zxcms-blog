<?php

namespace common\modules\blog\forms\search;

use common\modules\blog\entities\BlogCategory;
use common\modules\blog\entities\BlogPost;
use common\modules\blog\helpers\BlogPostHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class BlogPostSearch extends Model
{
    public $id;
    public $title;
    public $status;
    public $category_id;

    public function rules(): array
    {
        return [
            [['id', 'status', 'category_id'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = BlogPost::find()->typePublic()->joinWith('translation');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'category_id' => $this->category_id,
        ]);

        $query
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(BlogCategory::find()->orderBy('sort')->all(), 'id', function (BlogCategory $category) {
            return $category->translation->name;
        });
    }

    public function statusList(): array
    {
        return BlogPostHelper::statusList();
    }
}
