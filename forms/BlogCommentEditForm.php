<?php

namespace common\modules\blog\forms;

use common\modules\blog\entities\BlogComment;
use yii\base\Model;
use Yii;

class BlogCommentEditForm extends Model
{
    public $parentId;
    public $text;

    public function __construct(BlogComment $comment, $config = [])
    {
        $this->parentId = $comment->parent_id;
        $this->text = $comment->text;
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['text'], 'required'],
            ['text', 'string'],
            ['parentId', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'parentId' => Yii::t('blog', 'Parent Comment ID'),
            'text' => Yii::t('blog', 'Comment'),
        ];
    }
}
