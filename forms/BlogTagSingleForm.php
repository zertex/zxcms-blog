<?php

namespace common\modules\blog\forms;

use common\modules\blog\entities\BlogTag;
use core\validators\SlugValidator;
use yii\base\Model;
use Yii;

class BlogTagSingleForm extends Model
{
    public $name;
    public $slug;
    public $type_id;

    private $_tag;

    public function __construct(BlogTag $tag = null, $config = [])
    {
        if ($tag) {
            $this->name = $tag->name;
            $this->slug = $tag->slug;
            $this->_tag = $tag;
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [
                ['name', 'slug'],
                'unique',
                'targetClass' => BlogTag::class,
                'filter'      => $this->_tag ? ['<>', 'id', $this->_tag->id] : null
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('blog', 'Tag Name'),
            'slug' => Yii::t('blog', 'SEO link'),
        ];
    }

    public function attributeHints()
    {
        return [
            'slug' => Yii::t('pages', 'SEO link will be generated automatically if not specified'),
        ];
    }
}
