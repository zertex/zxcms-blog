<?php

namespace common\modules\blog\forms;

use common\modules\blog\entities\BlogPost;
use common\modules\blog\entities\BlogTag;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * @property array $newNames
 */
class BlogTagForm extends Model
{
    public $existing = [];
    public $textNew;
    public $new_tags;

    public function __construct(BlogPost $post = null, $config = [])
    {
        if ($post) {
            $this->new_tags = ArrayHelper::map($post->blogTagAssignments, 'tag_id', 'tag.name');
        }
        parent::__construct($config);
    }

    public function rules(): array
    {
        return [
            ['existing', 'each', 'rule' => ['integer']],
            [['textNew'], 'string'],
            ['existing', 'default', 'value' => []],
            ['new_tags', 'each', 'rule' => ['string']],
        ];
    }

    public function tagsList(): array
    {
        return ArrayHelper::map(BlogTag::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    public function getNewNames(): array
    {
        return array_filter(array_map('trim', preg_split('#\s*,\s*#i', $this->textNew)));
    }
}
