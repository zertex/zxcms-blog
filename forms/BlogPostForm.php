<?php

namespace common\modules\blog\forms;

use common\modules\blog\entities\BlogCategory;
use common\modules\blog\entities\BlogPost;
use core\forms\CompositeForm;
use core\forms\CompositeLanguageForm;
use core\forms\MetaForm;
use core\validators\SlugValidator;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use Yii;

/**
 * @property MetaForm $meta
 * @property BlogTagForm $tags
 */
class BlogPostForm extends CompositeLanguageForm
{
    public $type;

    public $category_id;
    public $title;
    public $description;
    public $content;
    public $image;
    public $video;
    public $published_at;
    public $slug;
    public $reset_image;
    public $status;
    public $meta_title;
    public $meta_description;
    public $meta_keywords;

    public $_post;

    public function __construct(BlogPost $post = null, array $attributes = [], $config = [])
    {
        if ($post) {
            $this->category_id  = $post->category_id;
            $this->video        = $post->video;
            $this->published_at = $post->published_at;
            $this->slug         = $post->slug;
            $this->tags         = new BlogTagForm($post);
            $this->status       = $post->status;
            $this->_post        = $post;
        } else {
            $this->tags   = new BlogTagForm();
            $this->status = 0;
        }
        parent::__construct($post, $attributes, $config);
    }

    public function rules(): array
    {
        return array_merge(
            parent::rules(),
            [
                [['category_id', 'title'], 'required'],
                [['title', 'video', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
                [['category_id', 'status'], 'integer'],
                [['description', 'content', 'meta_description'], 'string'],
                [['image'], 'image'],
                ['reset_image', 'boolean'],
                ['published_at', 'safe'],
                ['slug', SlugValidator::class],
                [
                    ['slug'],
                    'unique',
                    'targetClass' => BlogPost::class,
                    'filter'      => function (ActiveQuery $query) {
                        if ($this->type != BlogPost::TYPE_PUBLIC) {
                            $query->andWhere($this->type . '=' . BlogPost::TYPE_PUBLIC);
                        }

                        $query->andWhere(['type' => BlogPost::TYPE_PUBLIC]);
                        if ($this->_post) {
                            $query->andWhere(['<>', 'id', $this->_post->id]);
                        }

                        return $query;
                    }
                ],
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'id'               => Yii::t('blog', 'ID'),
                'category_id'      => Yii::t('blog', 'Category'),
                'published_at'     => Yii::t('blog', 'Published At'),
                'created_at'       => Yii::t('blog', 'Created At'),
                'updated_at'       => Yii::t('blog', 'Updated At'),
                'title'            => Yii::t('blog', 'Title'),
                'description'      => Yii::t('blog', 'Description'),
                'content'          => Yii::t('blog', 'Content'),
                'image'            => Yii::t('blog', 'Image'),
                'video'            => Yii::t('blog', 'Video'),
                'status'           => Yii::t('blog', 'Status'),
                'comments_count'   => Yii::t('blog', 'Comments Count'),
                'views'            => Yii::t('blog', 'Views'),
                'slug'             => Yii::t('blog', 'Slug'),
                'reset_image'      => Yii::t('blog', 'Reset Image'),
                'meta_title'       => Yii::t('blog', 'META Title'),
                'meta_description' => Yii::t('blog', 'META Description'),
                'meta_keywords'    => Yii::t('blog', 'META Keywords'),
            ]
        );
    }

    public function attributeHints()
    {
        return array_merge(
            parent::attributeHints(),
            [
                'published_at' => Yii::t('blog', 'The article will be published after the specified date if its status is not a draft'),
                'slug'         => Yii::t('pages', 'SEO link will be generated automatically if not specified'),
            ]
        );
    }

    public function categoriesList(): array
    {
        return ArrayHelper::map(BlogCategory::find()->orderBy('sort')->all(), 'id', function (BlogCategory $category) {
            return $category->translation->name;
        });
    }

    protected function internalForms(): array
    {
        return ['tags'];
    }

    public function beforeValidate(): bool
    {
        if (parent::beforeValidate()) {
            $this->image        = UploadedFile::getInstance($this, 'image');
            $this->published_at = strtotime($this->published_at);

            return true;
        }

        return false;
    }
}
