<?php

namespace common\modules\blog\urls;

use yii\caching\TagDependency;
use common\modules\blog\repositories\read\BlogCategoryReadRepository;
use yii\base\BaseObject;
use core\helpers\LanguageHelper;
use yii\caching\Cache;
use yii\web\UrlNormalizerRedirectException;
use yii\web\UrlRuleInterface;

class BlogCategoryUrlRule extends BaseObject implements UrlRuleInterface
{
    public $prefix = 'blog/category';

    private $_repository;
    private $_cache;

    public function __construct(BlogCategoryReadRepository $repository, Cache $cache, $config = [])
    {
        parent::__construct($config);
        $this->_repository = $repository;
        $this->_cache      = $cache;
    }

    public function parseRequest($manager, $request)
    {
        $uri = ltrim(LanguageHelper::processLangInUrl($request->pathInfo), '/');
        if (preg_match('#^' . $this->prefix . '/(.*[a-z])$#is', $uri, $matches)) {
            $path = $matches['1'];

            $result = $this->_cache->getOrSet(['blog_category_route', 'path' => $path], function () use ($path) {
                if (!$post = $this->_repository->findBySlug($this->getPathSlug($path))) {
                    return ['id' => null, 'path' => null];
                }

                return ['id' => $post->id, 'path' => $post->slug];
            }, null, new TagDependency(['tags' => ['blog']]));

            if (empty($result['id'])) {
                return false;
            }

            if ($path != $result['path']) {
                throw new UrlNormalizerRedirectException([
                    'blog/post/category',
                    'id' => $result['id'],
                ], 301);
            }

            return ['blog/post/category', ['id' => $result['id']]];
        }

        return false;
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'blog/post/category') {
            if (empty($params['id'])) {
                throw new \InvalidArgumentException('Empty id.');
            }
            $id = $params['id'];

            $url = $this->_cache->getOrSet(['blog_category_route', 'id' => $id], function () use ($id) {
                if (!$post = $this->_repository->find($id)) {
                    return null;
                }

                return $post->slug;
                //return $this->getPagePath($post);
            }, null, new TagDependency(['tags' => ['blog']]));

            if (!$url) {
                throw new \InvalidArgumentException('Undefined id.');
            }

            $url = $this->prefix . '/' . $url;
            unset($params['id']);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }

            //return $url;
            return LanguageHelper::addLangToUrl($url, isset($params['language']) ? $params['language'] : null);
        }

        return false;
    }

    private function getPathSlug($path): string
    {
        $chunks = explode('/', $path);

        return end($chunks);
    }
}
