<?php

namespace common\modules\blog\urls;

use common\modules\blog\repositories\read\BlogPostReadRepository;
use yii\base\BaseObject;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\web\UrlNormalizerRedirectException;
use yii\web\UrlRuleInterface;
use InvalidArgumentException;
use core\helpers\LanguageHelper;

class BlogMainUrlRule extends BaseObject implements UrlRuleInterface
{
    public $prefix = 'blog/post';

    private $_repository;
    private $_cache;

    public function __construct(BlogPostReadRepository $repository, Cache $cache, $config = [])
    {
        parent::__construct($config);
        $this->_repository = $repository;
        $this->_cache      = $cache;
    }

    public function parseRequest($manager, $request)
    {
        $uri = ltrim(LanguageHelper::processLangInUrl($request->pathInfo), '/');
        if (preg_match('#^' . $this->prefix . '/(.*[0-1a-z\_\-])$#is', $uri, $matches)) {
            $path = $matches['1'];

            $result = $this->_cache->getOrSet(['blog_main_route', 'path' => $path], function () use ($path) {
                if (!$post = $this->_repository->findBySlug($this->getPathSlug($path))) {
                    return ['id' => null, 'path' => null];
                }

                return ['id' => $post->id, 'path' => $post->slug];
            }, null, new TagDependency(['tags' => ['blog']]));

            if (empty($result['id'])) {
                return false;
            }

            if ($path != $result['path']) {
                throw new UrlNormalizerRedirectException(['blog/post/post', 'id' => $result['id']], 301);
            }
            //print_r($result['id']);die;
            return ['blog/post/post', ['id' => $result['id']]];
        }

        return false;
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'blog/post/post') {
            if (empty($params['id'])) {
                throw new InvalidArgumentException('Empty id.');
            }
            $id = $params['id'];

            $url = $this->_cache->getOrSet(['post_main_route', 'id' => $id], function () use ($id) {
                if (!$post = $this->_repository->find($id)) {
                    return null;
                }

                return $post->slug;
            }, null, new TagDependency(['tags' => ['blog']]));

            if (!$url) {
                return '#';
                //throw new InvalidArgumentException('Undefined id.');
            }

            $url = $this->prefix . '/' . $url;
            unset($params['id']);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }

            //return $url;
            return LanguageHelper::addLangToUrl($url, isset($params['language']) ? $params['language'] : null);
        }

        return false;
    }

    private function getPathSlug($path): string
    {
        $chunks = explode('/', $path);

        return end($chunks);
    }
}
