<?php

namespace common\modules\blog\urls;

use yii\caching\TagDependency;
use common\modules\blog\repositories\read\BlogTagReadRepository;
use yii\base\BaseObject;
use yii\caching\Cache;
use yii\web\UrlNormalizerRedirectException;
use yii\web\UrlRuleInterface;

class BlogTagUrlRule extends BaseObject implements UrlRuleInterface
{
    public $prefix = 'blog/tag';

    private $_repository;
    private $_cache;

    public function __construct(BlogTagReadRepository $repository, Cache $cache, $config = [])
    {
        parent::__construct($config);
        $this->_repository = $repository;
        $this->_cache      = $cache;
    }

    public function parseRequest($manager, $request)
    {
        if (preg_match('#^' . $this->prefix . '/(.*[a-z])$#is', $request->pathInfo, $matches)) {
            $path = $matches['1'];

            $result = $this->_cache->getOrSet(['blog_tag_route', 'path' => $path], function () use ($path) {
                if (!$post = $this->_repository->findBySlug($this->getPathSlug($path))) {
                    return ['id' => null, 'path' => null];
                }

                return ['id' => $post->id, 'path' => $post->slug];
            }, null, new TagDependency(['tags' => ['blog']]));

            if (empty($result['id'])) {
                return false;
            }

            if ($path != $result['path']) {
                throw new UrlNormalizerRedirectException([
                    'blog/post/tag',
                    'id' => $result['id'],
                ], 301);
            }

            return ['blog/post/tag', ['id' => $result['id']]];
        }

        return false;
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route == 'blog/post/tag') {
            if (empty($params['id'])) {
                throw new \InvalidArgumentException('Empty id.');
            }
            $id = $params['id'];

            $url = $this->_cache->getOrSet(['blog_tag_route', 'id' => $id], function () use ($id) {
                if (!$post = $this->_repository->find($id)) {
                    return null;
                }

                return $post->slug;
                //return $this->getPagePath($post);
            }, null, new TagDependency(['tags' => ['blog']]));

            if (!$url) {
                throw new \InvalidArgumentException('Undefined id.');
            }

            $url = $this->prefix . '/' . $url;
            unset($params['id']);
            if (!empty($params) && ($query = http_build_query($params)) !== '') {
                $url .= '?' . $query;
            }

            return $url;
        }

        return false;
    }

    private function getPathSlug($path): string
    {
        $chunks = explode('/', $path);

        return end($chunks);
    }
}
