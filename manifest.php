<?php

return [
    'version'     => '1.0.1',
    'name'        => 'blog',
    'description' => 'Blog system for site with comments and slug',
    'module'      => 'BlogModule',
    'permissions' => [
        'BlogManagement' => 'Blog management system'
    ],
    'git_path'    => 'https://gitlab.com/zertex/zxcms-blog',
    'git_name'    => 'zxcms-blog',
    'enabled'     => true,
];
