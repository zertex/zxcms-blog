<?php

namespace common\modules\blog\services;

use common\modules\blog\entities\BlogCategory;
use common\modules\blog\forms\BlogCategoryForm;
use common\modules\blog\repositories\BlogCategoryRepository;
use common\modules\blog\repositories\BlogRepository;

class BlogCategoryManageService
{
    private $_categories;
    private $_posts;

    public function __construct(BlogCategoryRepository $categories, BlogRepository $posts)
    {
        $this->_categories = $categories;
        $this->_posts      = $posts;
    }

    public function create(BlogCategoryForm $form): BlogCategory
    {
        $category = BlogCategory::create(
            $form,
            $form->slug,
            $form->sort
        );
        $this->_categories->save($category);

        return $category;
    }

    public function edit($id, BlogCategoryForm $form): void
    {
        $category = $this->_categories->get($id);
        $category->edit(
            $form,
            $form->slug,
            $form->sort
        );
        $this->_categories->save($category);
    }

    public function remove($id): void
    {
        $category = $this->_categories->get($id);
        if ($this->_posts->existsByCategory($category->id)) {
            throw new \DomainException('Unable to remove category with posts.');
        }
        $this->_categories->remove($category);
    }
}
