<?php

namespace common\modules\blog\services;

use common\modules\blog\entities\BlogComment;
use common\modules\blog\forms\BlogCommentForm;
use common\modules\blog\repositories\BlogRepository;
use core\repositories\user\UserRepository;

class BlogCommentService
{
    private $_posts;
    private $_users;

    public function __construct(BlogRepository $posts, UserRepository $users)
    {
        $this->_posts = $posts;
        $this->_users = $users;
    }

    public function create($postId, $userId, BlogCommentForm $form): BlogComment
    {
        $post = $this->_posts->get($postId);
        $user = $this->_users->get($userId);

        $comment = $post->addComment($user->id, $form->parentId, $form->text);

        $this->_posts->save($post);

        return $comment;
    }
}
