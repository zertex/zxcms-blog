<?php

namespace common\modules\blog\services;

use common\modules\blog\entities\BlogTag;
use common\modules\blog\forms\BlogTagSingleForm;
use common\modules\blog\repositories\BlogTagRepository;

class BlogTagManageService
{
    private $_tags;

    public function __construct(BlogTagRepository $tags)
    {
        $this->_tags = $tags;
    }

    public function create(BlogTagSingleForm $form): BlogTag
    {
        $tag = BlogTag::create(
            $form->name,
            $form->slug
        );
        $this->_tags->save($tag);
        return $tag;
    }

    public function edit($id, BlogTagSingleForm $form): void
    {
        $tag = $this->_tags->get($id);
        $tag->edit(
            $form->name,
            $form->slug
        );
        $this->_tags->save($tag);
    }

    public function remove($id): void
    {
        $tag = $this->_tags->get($id);
        $this->_tags->remove($tag);
    }
}
