<?php

namespace common\modules\blog\services;

use common\modules\blog\forms\BlogCommentEditForm;
use common\modules\blog\repositories\BlogRepository;

class BlogCommentManageService
{
    private $_posts;

    public function __construct(BlogRepository $posts)
    {
        $this->_posts = $posts;
    }

    public function edit($postId, $id, BlogCommentEditForm $form): void
    {
        $post = $this->_posts->get($postId);
        $post->editComment($id, $form->parentId, $form->text);
        $this->_posts->save($post);
    }

    public function activate($postId, $id): void
    {
        $post = $this->_posts->get($postId);
        $post->activateComment($id);
        $this->_posts->save($post);
    }

    public function remove($postId, $id): void
    {
        $post = $this->_posts->get($postId);
        $post->removeComment($id);
        $this->_posts->save($post);
    }
}
