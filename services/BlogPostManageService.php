<?php

namespace common\modules\blog\services;

use common\modules\blog\entities\BlogPost;
use common\modules\blog\entities\BlogTag;
use common\modules\blog\forms\BlogPostForm;
use common\modules\blog\helpers\BlogPostHelper;
use common\modules\blog\repositories\BlogCategoryRepository;
use common\modules\blog\repositories\BlogRepository;
use common\modules\blog\repositories\BlogTagRepository;
use core\helpers\VideoHelper;
use core\services\TransactionManager;
use yii\base\Security;
use yii\helpers\Inflector;

class BlogPostManageService
{
    private $_posts;
    private $_categories;
    private $_tags;
    private $_transaction;

    public function __construct(
        BlogRepository $posts,
        BlogCategoryRepository $categories,
        BlogTagRepository $tags,
        TransactionManager $transaction
    ) {
        $this->_posts       = $posts;
        $this->_categories  = $categories;
        $this->_tags        = $tags;
        $this->_transaction = $transaction;
    }

    public function create(BlogPostForm $form, $type = BlogPost::TYPE_PUBLIC): BlogPost
    {
        $category = $this->_categories->get($form->category_id);

        $post = BlogPost::create(
            $form,
            $category->id,
            $form->slug,
            $form->published_at,
            $form->video,
            $type
        );

        $this->_transaction->wrap(function () use ($post, $form) {

            if (is_array($form->tags->new_tags) && !empty($form->tags->new_tags)) {
                foreach ($form->tags->new_tags as $tag_id => $tag_name) {
                    if (!$tag = $this->_tags->findByName($tag_name)) {
                        $tag = BlogTag::create($tag_name, Inflector::slug($tag_name, '_'));
                        $this->_tags->save($tag);
                    }
                    $post->assignTag($tag->id);
                }
            }
            $this->_posts->save($post);

            if ($form->image) {
                $post->setImage($form->image);
            } elseif ($form->video) {
                $src = VideoHelper::getThumb($form->video);
                //$src = 'https://i.ytimg.com/vi/' . BlogPostHelper::parseYoutubeUrl($post->video) . '/maxresdefault.jpg';
                $filename = (new Security())->generateRandomString(15) . '.jpg';
                copy($src, \Yii::getAlias(BlogPost::FILE_ORIGINAL_PATH . '/' . $post->id . '.jpg'));
                $post->image = $filename;
            }

            if ($form->status == BlogPost::STATUS_ACTIVE) {
                $post->activate();
            }

            $this->_posts->save($post);
        });

        return $post;
    }

    public function edit($id, BlogPostForm $form): void
    {
        $post = $this->_posts->get($id);
        BlogPostHelper::saveRevision(clone $post);
        //$this->saveRevision(clone $post);

        //print_r($post->translations); die;

        $category = $this->_categories->get($form->category_id);

        $post->edit(
            $form,
            $category->id,
            $form->slug,
            $form->published_at,
            $form->video,
            $post->type
        );

        if ($form->image) {
            $post->cleanFiles();
            $post->setImage($form->image);
        } elseif ($form->video && (!$post->image || $form->reset_image)) {
            $post->cleanFiles();
            $src      = VideoHelper::getThumb($form->video);
            $filename = (new Security())->generateRandomString(15) . '.jpg';
            copy($src, \Yii::getAlias(BlogPost::FILE_ORIGINAL_PATH . '/' . $post->id . '.jpg'));
            $post->image = $filename;
        } elseif ($post->image && $form->reset_image) {
            $post->cleanFiles();
            $post->image = null;
            $post->updateAttributes(['image']);
        }

        $this->_transaction->wrap(function () use ($post, $form) {
            $post->revokeTags();
            $this->_posts->save($post);

            if (is_array($form->tags->new_tags) && !empty($form->tags->new_tags)) {
                foreach ($form->tags->new_tags as $tag_id => $tag_name) {
                    if (!$tag = $this->_tags->findByName($tag_name)) {
                        $tag = BlogTag::create($tag_name, Inflector::slug($tag_name, '_'));
                        $this->_tags->save($tag);
                    }
                    $post->assignTag($tag->id);
                }
            }
            $this->_posts->save($post);
        });
    }

    public function activate($id): void
    {
        $post = $this->_posts->get($id);
        $post->activate();
        $this->_posts->save($post);
    }

    public function draft($id): void
    {
        $post = $this->_posts->get($id);
        $post->draft();
        $this->_posts->save($post);
    }

    public function remove($id): void
    {
        $post = $this->_posts->get($id);
        // Remove revisions
        $this->clearHistory($post);
        // Remove preview
        $this->removePreviews();

        $this->_posts->remove($post);
    }

    public function removePreviews(): void
    {
        $posts = BlogPost::find()->andWhere(['type' => BlogPost::TYPE_PREVIEW])->all();
        foreach ($posts as $post) {
            $post->delete();
        }
    }

    public function clearHistory(BlogPost $post): void
    {
        BlogPost::deleteAll(['revision_id' => $post->id]);
    }

    public function restoreHistory($from_id, $id): int
    {
        $post = $this->_posts->get($id);
        $from = $this->_posts->get($from_id);

        // update revision id
        BlogPost::updateAll(['revision_id' => $from->id], ['revision_id' => $post->id]);

        $this->_posts->remove($post);
        $from->revision_id = null;
        $from->type        = BlogPost::TYPE_PUBLIC;
        $this->_posts->save($from);

        // delete never revisions
        BlogPost::deleteAll(['AND', ['revision_id' => $from->id], ['>', 'revision_at', $from->revision_at]]);

        return $from->id;
    }
}
