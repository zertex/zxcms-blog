<?php

use yii\db\Migration;

/**
 * Class m180725_113503_remove_blog_posts_slug_unique_index
 */
class m180725_113503_remove_blog_posts_slug_unique_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('{{%idx-blog_posts-slug}}', '{{%blog_posts}}');
        $this->createIndex('{{%idx-blog_posts-slug}}', '{{%blog_posts}}', 'slug');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx-blog_posts-slug}}', '{{%blog_posts}}');
        $this->createIndex('{{%idx-blog_posts-slug}}', '{{%blog_posts}}', 'slug');
    }
}
