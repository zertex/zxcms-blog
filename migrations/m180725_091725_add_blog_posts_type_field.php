<?php

use yii\db\Migration;

/**
 * Class m180725_091725_add_blog_posts_type_field
 */
class m180725_091725_add_blog_posts_type_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%blog_posts}}', 'type', $this->integer(2)->defaultValue(0)); // 0 - public, 1 - revision, 2 - preview
        $this->addColumn('{{%blog_posts}}', 'revision_at', $this->integer()->unsigned());
        $this->addColumn('{{%blog_posts}}', 'revision_id', $this->integer());

        $this->createIndex('idx_blog_posts_revision_id', '{{%blog_posts}}', 'revision_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_blog_posts_revision_id', '{{%blog_posts}}');

        $this->dropColumn('{{%blog_posts}}', 'type');
        $this->dropColumn('{{%blog_posts}}', 'revision_at');
        $this->dropColumn('{{%blog_posts}}', 'revision_id');
    }
}
