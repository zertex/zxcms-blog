<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_categories_lng`.
 */
class m180830_163050_create_blog_categories_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%blog_categories_lng}}', [
            'id'               => $this->primaryKey(),
            'blog_category_id' => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'name'             => $this->string()->notNull(),
            'title'            => $this->string(),
            'description'      => $this->text(),
            'meta_title'       => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords'    => $this->string(255),
        ], $tableOptions);

        $this->createIndex('idx_blog_categories_lng_language', '{{%blog_categories_lng}}', 'language');
        $this->createIndex('idx_blog_categories_lng_blog_category_id', '{{%blog_categories_lng}}', 'blog_category_id');
        $this->addForeignKey('frg_blog_categories_lng_blog_categories_blog_category_id_id', '{{%blog_categories_lng}}', 'blog_category_id', '{{%blog_categories}}', 'id', 'CASCADE', 'CASCADE');

        $this->dropColumn('{{%blog_categories}}', 'title');
        $this->dropColumn('{{%blog_categories}}', 'description');
        $this->dropColumn('{{%blog_categories}}', 'name');
        $this->dropColumn('{{%blog_categories}}', 'meta_json');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('frg_blog_categories_lng_blog_categories_blog_category_id_id', '{{%blog_categories_lng}}');
        $this->dropIndex('idx_blog_categories_lng_blog_category_id', '{{%blog_categories_lng}}');
        $this->dropIndex('idx_blog_categories_lng_language', '{{%blog_categories_lng}}');

        $this->dropTable('{{%blog_categories_lng}}');

        $this->addColumn('{{%blog_categories}}', 'title', $this->string(255));
        $this->addColumn('{{%blog_categories}}', 'name', $this->string(255)->notNull());
        $this->addColumn('{{%blog_categories}}', 'description', $this->text());
        $this->addColumn('{{%blog_categories}}', 'meta_json', 'LONGTEXT NOT NULL');
    }
}
