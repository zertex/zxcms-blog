<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_posts`.
 */
class m180605_190347_create_blog_posts_table extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%blog_posts}}', [
            'id'             => $this->primaryKey(),
            'category_id'    => $this->integer()->notNull(),
            'published_at'   => $this->integer()->unsigned()->notNull(),
            'created_at'     => $this->integer()->unsigned(),
            'updated_at'     => $this->integer()->unsigned(),
            'title'          => $this->string()->notNull(),
            'description'    => $this->text(),
            'content'        => 'MEDIUMTEXT',
            'image'          => $this->string(),
            'video'          => $this->string(),
            'status'         => $this->integer()->notNull(),
            'meta_json'      => 'TEXT NOT NULL',
            'comments_count' => $this->integer()->notNull()->defaultValue(0),
            'views'          => $this->integer()->notNull()->defaultValue(0),
            'slug'           => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-blog_posts-category_id}}', '{{%blog_posts}}', 'category_id');
        $this->createIndex('{{%idx-blog_posts-slug}}', '{{%blog_posts}}', 'slug', true);
        $this->addForeignKey('{{%fk-blog_posts-category_id}}', '{{%blog_posts}}', 'category_id', '{{%blog_categories}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('{{%fk-blog_posts-category_id}}', '{{%blog_posts}}');
        $this->dropTable('{{%blog_posts}}');
    }
}
