<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_tags`.
 */
class m180605_193423_create_blog_tags_table extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%blog_tags}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-blog_tags-slug}}', '{{%blog_tags}}', 'slug', true);
    }

    public function down()
    {
        $this->dropIndex('{{%idx-blog_tags-slug}}', '{{%blog_tags}}');
        $this->dropTable('{{%blog_tags}}');
    }
}
