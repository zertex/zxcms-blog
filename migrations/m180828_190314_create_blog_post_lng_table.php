<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_post_lng`.
 */
class m180828_190314_create_blog_post_lng_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%blog_posts_lng}}', [
            'id'               => $this->primaryKey(),
            'blog_post_id'     => $this->integer()->notNull(),
            'language'         => $this->string(6)->notNull(),
            'title'            => $this->string()->notNull(),
            'description'      => $this->text(),
            'content'          => 'MEDIUMTEXT',
            'meta_title'       => $this->string(255),
            'meta_description' => $this->text(),
            'meta_keywords'    => $this->string(255),
        ], $tableOptions);

        $this->createIndex('idx_blog_posts_lng_language', '{{%blog_posts_lng}}', 'language');
        $this->createIndex('idx_blog_posts_lng_blog_post_id', '{{%blog_posts_lng}}', 'blog_post_id');
        $this->addForeignKey('frg_blog_posts_lng_blog_posts_blog_post_id_id', '{{%blog_posts_lng}}', 'blog_post_id', '{{%blog_posts}}', 'id', 'CASCADE', 'CASCADE');

        $this->dropColumn('{{%blog_posts}}', 'title');
        $this->dropColumn('{{%blog_posts}}', 'description');
        $this->dropColumn('{{%blog_posts}}', 'content');
        $this->dropColumn('{{%blog_posts}}', 'meta_json');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%blog_posts}}', 'title', $this->string(255)->notNull());
        $this->addColumn('{{%blog_posts}}', 'description', $this->text());
        $this->addColumn('{{%blog_posts}}', 'content', 'MEDIUMTEXT');
        $this->addColumn('{{%blog_posts}}', 'meta_json', $this->text());

        $this->dropForeignKey('frg_blog_posts_lng_blog_posts_blog_post_id_id', '{{%blog_posts_lng}}');
        $this->dropIndex('idx_blog_posts_lng_blog_post_id', '{{%blog_posts_lng}}');
        $this->dropIndex('idx_blog_posts_lng_language', '{{%blog_posts_lng}}');
        $this->dropTable('{{%blog_posts_lng}}');
    }
}
