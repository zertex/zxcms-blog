<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_categories`.
 */
class m180605_184534_create_blog_categories_table extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%blog_categories}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'slug'        => $this->string()->notNull(),
            'title'       => $this->string(),
            'description' => $this->text(),
            'sort'        => $this->integer()->notNull(),
            'meta_json'   => 'LONGTEXT NOT NULL',
        ], $tableOptions);

        $this->createIndex('{{%idx-blog_categories-slug}}', '{{%blog_categories}}', 'slug', true);
    }

    public function down()
    {
        $this->dropTable('{{%blog_categories}}');
    }
}
