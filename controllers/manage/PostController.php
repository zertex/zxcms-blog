<?php

namespace common\modules\blog\controllers\manage;

use common\modules\blog\entities\BlogPost;
use common\modules\blog\entities\BlogTag;
use common\modules\blog\forms\BlogPostForm;
use common\modules\blog\forms\search\BlogPostSearch;
use common\modules\blog\services\BlogPostManageService;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;

class PostController extends Controller
{
    private $_service;

    public function __construct($id, $module, BlogPostManageService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->_service = $service;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['BlogManagement'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete'          => ['POST'],
                    'activate'        => ['POST'],
                    'draft'           => ['POST'],
                    'delete-photo'    => ['POST'],
                    'move-photo-up'   => ['POST'],
                    'move-photo-down' => ['POST'],
                    'restore-history' => ['POST'],
                    'clear-history'   => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel  = new BlogPostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $post = $this->findModel($id);

        $history = BlogPost::find()
                           ->andWhere(['OR', ['revision_id' => $id], ['id' => $id]])
                           ->orderBy(['revision_at' => SORT_DESC])
                           ->limit(20)
                           ->all();

        return $this->render('view', [
            'post'    => $post,
            'history' => $history,
        ]);
    }

    public function actionCreatePreview($id = null, $language = '')
    {
        $this->_service->removePreviews();

        $form       = new BlogPostForm();
        $form->type = BlogPost::TYPE_PREVIEW;
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $post = $this->_service->create($form, BlogPost::TYPE_PREVIEW);
                if ($id && !$post->image) {
                    $real_post = BlogPost::findOne($id);
                    if ($real_post->image) {
                        $post->image = $real_post->image;
                        $post->save();
                        $path  = Yii::getAlias('@staticRoot/origin/posts');
                        $parts = pathinfo($real_post->image);
                        copy($path . '/' . $real_post->id . '.' . $parts['extension'], $path . '/' . $post->id . '.' . $parts['extension']);
                    }
                }

                return $this->redirect(Url::to(Yii::$app->get('frontendUrlManager')->createAbsoluteUrl([
                    '/blog/post/preview',
                    'id'       => $post->id,
                    'language' => $language
                ])));
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        $form->published_at = date('d.m.Y H:i:s');

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionCreate()
    {
        $form = new BlogPostForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $post = $this->_service->create($form);

                return $this->redirect(['view', 'id' => $post->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }
        $form->published_at = date('d.m.Y H:i:s');

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $post = $this->findModel($id);
        $form = new BlogPostForm($post);

        $form->published_at = date('d.m.Y H:i:s', $form->published_at);
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->_service->edit($post->id, $form);

                return $this->redirect(['view', 'id' => $post->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('update', [
            'model' => $form,
            'post'  => $post,
        ]);
    }

    /**
     * @param $id
     *
     * @return Response
     */
    public function actionDelete($id)
    {
        try {
            $this->_service->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     */
    public function actionActivate($id)
    {
        try {
            $this->_service->activate($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDraft($id)
    {
        try {
            $this->_service->draft($id);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionTagSearch($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out                         = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data           = BlogTag::find()->select('name as id, name as text')->andWhere([
                'like',
                'name',
                $q
            ])->orderBy('name')->limit(20)->asArray()->all();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $tag_name       = BlogTag::findOne($id)->name;
            $out['results'] = ['id' => $tag_name, 'text' => $tag_name];
        }

        return $out;
    }

    public function actionPostSearch($q = null, $id = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out                         = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data  = [];
            $posts = BlogPost::find()
                             ->with('translation')
                             ->leftJoin('{{%blog_posts_lng}}', '`blog_posts_lng`.`blog_post_id` = `blog_posts`.`id`')
                             ->andWhere(['like', 'blog_posts_lng.title', $q])
                             ->limit(20)
                             ->all();

            foreach ($posts as $post) {
                $data[] = [
                    'id'   => $post->id,
                    'text' => isset($post->translation) ? $post->translation->title : null,
                ];
            }
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $tag_name       = BlogPost::findOne($id)->translation->title;
            $out['results'] = ['id' => $tag_name, 'text' => $tag_name];
        } else {
            $data  = [];
            $posts = BlogPost::find()->typePublic()->orderBy(['id' => SORT_DESC])->limit(20)->all();

            foreach ($posts as $post) {
                $data[] = [
                    'id'   => $post->id,
                    'text' => isset($post->translation) ? $post->translation->title : null,
                ];
            }
            $out['results'] = array_values($data);
        }

        return $out;
    }

    public function actionRestoreHistory($id)
    {
        $post = $this->findModel($id);
        if ($post_id = $post->revision_id) {
            $new_id = $this->_service->restoreHistory($id, $post->revision_id);

            return $this->redirect(['/blog/manage/post/view', 'id' => $new_id]);
        }

        return $this->redirect(['/blog/manage/post/index']);
    }

    public function actionClearHistory($id)
    {
        $post = $this->findModel($id);
        $this->_service->clearHistory($post);

        return $this->redirect(['/blog/manage/post/view', 'id' => $post->id]);
    }

    /**
     * @param $id
     *
     * @return BlogPost
     * @throws NotFoundHttpException
     */
    protected function findModel($id): BlogPost
    {
        if (($model = BlogPost::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findLanguageModel($id)
    {
        if (($model = BlogPost::find()->multilingual()->andWhere(['id' => $id])->one()) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
