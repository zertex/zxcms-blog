<?php

namespace common\modules\blog\controllers;

use common\modules\blog\forms\BlogCommentForm;
use common\modules\blog\repositories\read\BlogCategoryReadRepository;
use common\modules\blog\repositories\read\BlogPostReadRepository;
use common\modules\blog\repositories\read\BlogTagReadRepository;
use common\modules\blog\services\BlogCommentService;
use frontend\components\FrontendController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class PostController extends FrontendController
{
    public $layout = 'blog';

    private $_service;
    private $_posts;
    private $_categories;
    private $_tags;

    public function __construct(
        $id,
        $module,
        BlogCommentService $service,
        BlogPostReadRepository $posts,
        BlogCategoryReadRepository $categories,
        BlogTagReadRepository $tags,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->_service    = $service;
        $this->_posts      = $posts;
        $this->_categories = $categories;
        $this->_tags       = $tags;
    }

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'category', 'tag', 'post'],
                        'allow'   => true,
                        //'roles' => ['Blog'],
                    ],
                    [
                        'actions' => ['preview'],
                        'allow'   => true,
                        'roles'   => ['BlogManagement'],
                    ],
                    [
                        'actions' => ['comment'],
                        'allow'   => true,
                        'roles'   => ['Comments'],
                    ],
                    [    // all the action are accessible to admin
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        /* @var $dataProvider ActiveDataProvider */
        $dataProvider                       = $this->_posts->getAll();
        $dataProvider->pagination->pageSize = 10;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCategory($id)
    {
        if (!$category = $this->_categories->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $dataProvider = $this->_posts->getAllByCategory($category);

        return $this->render('category', [
            'category'     => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionTag($id)
    {
        if (!$tag = $this->_tags->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $dataProvider = $this->_posts->getAllByTag($tag);

        return $this->render('tag', [
            'tag'          => $tag,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPost($id)
    {
        if (!$post = $this->_posts->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('post', [
            'post' => $post,
        ]);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionComment($id)
    {
        if (!$post = $this->_posts->find($id)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $form = new BlogCommentForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $comment = $this->_service->create($post->id, Yii::$app->user->id, $form);

                return $this->redirect(['post', 'id' => $post->id, '#' => 'comment_' . $comment->id]);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        return $this->render('comment', [
            'post'  => $post,
            'model' => $form,
        ]);
    }

    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if ($action->id === 'preview') {
            # code...
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /*public function actionPreview($id = null)
    {
        Yii::$app->controller->enableCsrfValidation = false;
        $form = new BlogPostForm();
        $parent = $id ? BlogPost::findOne($id) : null;

        if ($form->load(Yii::$app->request->post())) {
            $form->slug = md5(time());
            if ($form->validate()) {
                $post               = new BlogPost();
                $post->id           = - 1;
                $post->title        = $form->title;
                $post->description  = $form->description;
                $post->content      = $form->content;
                $post->category_id  = $form->category_id;
                $post->created_at   = time();
                $post->updated_at   = time();
                $post->published_at = time();

                $meta            = new Meta( $form->meta->title, $form->meta->description, $form->meta->keywords );
                $post->meta      = $meta;
                $post->meta_json = Json::encode( [
                    'title'       => $form->meta->title,
                    'description' => $form->meta->description,
                    'keywords'    => $form->meta->keywords,
                ] );
                $post->video     = $form->video;
                $post->slug      = $form->slug;

                if ( $form->image ) {
                    $post->setImage( $form->image );
                } else if ( $form->video ) {
                    $src      = 'https://i.ytimg.com/vi/' . BlogPostHelper::parseYoutubeUrl( $post->video ) . '/maxresdefault.jpg';
                    $filename = ( new Security() )->generateRandomString( 15 ) . '.jpg';
                    copy( $src, \Yii::getAlias( BlogPost::FILE_ORIGINAL_PATH . '/' . $post->id . '.jpg' ) );
                    //copy($src, \Yii::getAlias(BlogPost::FILE_ORIGINAL_PATH . '/' . $filename));
                    $post->image = $filename;
                }

                if ( $post->image ) {
                    $path = $post->getUploadedFilePath( 'image' );
                    FileHelper::createDirectory( pathinfo( $path, PATHINFO_DIRNAME ), 0775, true );
                    $post->image->saveAs( $path );
                    $post->image = $post->getImageFileUrl( 'image' );
                } else {
                    $post->image = $parent->image;
                    //$post->image = $post->getImageFileUrl( 'image' );
                }

                return $this->render( 'post', [
                    'post' => $post,
                ] );
            }
        } else {print_r($form->errors);}
        return '';
    }*/

    public function actionPreview($id, $language = '')
    {
        if (!$post = $this->_posts->findPreview($id, $language)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        /*if ($language) {
            Yii::$app->language = $language;
        }*/

        return $this->render('post', [
            'post' => $post,
        ]);
    }
}
