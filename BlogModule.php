<?php

namespace common\modules\blog;

use common\modules\blog\widgets\MenuItemCreatorWidget;
use core\components\modules\ModuleInterface;
use yii\helpers\ArrayHelper;

/**
 * blog module definition class
 */
class BlogModule extends \yii\base\Module implements ModuleInterface
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\blog\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }

    public function bootstrap($app)
    {
        // add migration path
        $app->controllerMap['migrate']['migrationPath'][] = '@common/modules/blog/migrations';

        // add search rules
        $app->params['search_rules'][] = "SELECT title, content, CONCAT('/blog/manage/post/view/', blog_post_id) AS url FROM {{blog_posts_lng}}";

        // add rules
        $app->getUrlManager()->addRules([
            'blog' => 'blog/post/index',
        ]);

        $app->getUrlManager()->addRules([
            ['class' => 'common\modules\blog\urls\BlogMainUrlRule'],
            ['class' => 'common\modules\blog\urls\BlogCategoryUrlRule'],
            ['class' => 'common\modules\blog\urls\BlogTagUrlRule'],
        ]);

        $app->getUrlManager()->addRules([
            'blog/manage/post/view/<id:\d+>' => 'blog/manage/post/view',
        ]);

        // add languages
        $app->getI18n()->translations = ArrayHelper::merge($app->getI18n()->translations, [
            'blog'        => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/blog/messages',
            ],
            'blog_public' => [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => '@common/modules/blog/messages',
            ],
        ]);

        // add menu items
        /*$app->params['adminMenu'][] = [
            'label' => \Yii::t('blog', 'Blog'), 'icon' => 'book', 'items' => [
                ['label' => \Yii::t('blog', 'Categories'), 'icon' => 'caret-right', 'url' => ['/blog/manage/category/index'], 'active' => \Yii::$app->controller->getUniqueId() == 'blog/manage/category'],
                ['label' => \Yii::t('blog', 'Posts'), 'icon' => 'caret-right', 'url' => ['/blog/manage/post/index'], 'active' => \Yii::$app->controller->getUniqueId() == 'blog/manage/post'],
            ], 'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('BlogManagement')
        ];*/
        //print_r(basename($app->getBasePath())); die;
        //print_r($app->basePath); die;

        if (basename($app->getBasePath()) === 'backend') {
            $app->params['adminMenu'][] = [
                'label'   => \Yii::t('blog', 'Blog'),
                'icon'    => 'book',
                'items'   => [
                    [
                        'label' => \Yii::t('blog', 'Categories'),
                        'icon'  => 'caret-right',
                        'url'   => ['/blog/manage/category/index']
                    ],
                    [
                        'label' => \Yii::t('blog', 'Posts'),
                        'icon'  => 'caret-right',
                        'url'   => ['/blog/manage/post/index']
                    ],
                    [
                        'label' => \Yii::t('blog', 'Comments'),
                        'icon'  => 'caret-right',
                        'url'   => ['/blog/manage/comment/index']
                    ],
                    [
                        'label' => \Yii::t('blog', 'Tags'),
                        'icon'  => 'caret-right',
                        'url'   => ['/blog/manage/tag/index']
                    ],
                ],
                'visible' => \Yii::$app->user->can('admin') || \Yii::$app->user->can('BlogManagement')
            ];
        }
    }

    public static function getMenuItemCreator($menu_id): array
    {
        $widgets   = [];
        $widgets[] = [
            'id'      => 'blog',
            'title'   => \Yii::t('blog', 'Blog'),
            'content' => MenuItemCreatorWidget::widget([
                'menu_id' => $menu_id,
            ]),
        ];

        return $widgets;
    }
}
