<?php

return [
	'Blog' => 'Блог',
	'Read More' => 'Подробнее',
	'Leave a Comment' => 'Комментировать',
	'Categories' => 'Категории',
];