<?php

namespace common\modules\blog\helpers;

use common\modules\blog\entities\BlogPost;
use common\modules\blog\forms\BlogPostForm;
use common\modules\blog\repositories\BlogRepository;
use common\modules\blog\repositories\read\BlogPostReadRepository;
use common\modules\blog\services\BlogPostManageService;
use core\dispatchers\DeferredEventDispatcher;
use core\entities\post\Post;
use core\services\TransactionManager;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;

class BlogPostHelper
{
    public static function statusList(): array
    {
        return [
            Post::STATUS_DRAFT  => Yii::t('blog', 'Draft'),
            Post::STATUS_ACTIVE => Yii::t('blog', 'Active'),
        ];
    }

    public static function statusName($status): string
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLabel($status): string
    {
        switch ($status) {
            case Post::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case Post::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    public static function parseYoutubeUrl($url)
    {
        $urls = parse_url($url);
        //url is http://youtu.be/xxxx
        if ($urls['host'] == 'youtu.be') {
            $id = ltrim($urls['path'], '/');
        } //url is http://www.youtube.com/embed/xxxx
        elseif (strpos($urls['path'], 'embed') == 1) {
            $id = end(explode('/', $urls['path']));
        } //url is xxxx only
        elseif (strpos($url, '/') === false) {
            $id = $url;
        }
        //http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
        //url is http://www.youtube.com/watch?v=xxxx
        else {
            parse_str($urls['query']);
            /* @var $v */
            $id = $v;
            if (!empty($feature)) {
                $id = end(explode('v=', $urls['query']));
            }
        }

        return $id;
    }

    public static function saveRevision1(BlogPost $model)
    {
        if (!$model->revision_id) {
            $blogForm          = new BlogPostForm($model);
            $blog              = BlogPost::create(
                $blogForm,
                $model->category_id,
                $model->slug,
                $model->published_at,
                $model->video,
                $model->type
            );
            $blog->type        = BlogPost::TYPE_REVISION;
            $blog->revision_at = $model->updated_at;
            $blog->revision_id = $model->id;

            foreach ($model->tags as $tag) {
                $blog->assignTag($tag->id);
            }

            $blog->save();

            if ($model->image) {
                $path  = Yii::getAlias('@staticRoot/origin/posts');
                $parts = pathinfo($model->image);
                copy($path . '/' . $model->id . '.' . $parts['extension'], $path . '/' . $blog->id . '.' . $parts['extension']);
            }
        }
    }

    public static function saveRevision(BlogPost $model)
    {
        if (!$model->revision_id) {
            $revision              = clone $model;
            $revision->id          = null;
            $revision->isNewRecord = true;
            $revision->revision_at = $revision->updated_at;
            $revision->revision_id = $model->id;
            $revision->type        = BlogPost::TYPE_REVISION;
            $revision->_form       = new BlogPostForm($model);
            $revision->save();

            // tags
            foreach ($model->tags as $tag) {
                $revision->assignTag($tag->id);
            }
            $revision->save();

            if ($model->image) {
                $path  = Yii::getAlias('@staticRoot/origin/posts');
                $parts = pathinfo($model->image);
                copy($path . '/' . $model->id . '.' . $parts['extension'], $path . '/' . $revision->id . '.' . $parts['extension']);
            }
        }
    }
}
