<?php

namespace common\modules\blog\entities;

use common\modules\blog\entities\queries\BlogCategoryQuery;
use core\behaviors\LanguageBehavior;
use core\behaviors\SluggableRelationBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property integer $id
 * @property string $slug
 * @property integer $sort
 *
 *
 * @method ActiveRecord findTranslation(string $language)
 * @method void saveTranslations($translations)
 *
 * @property ActiveRecord[] $translations
 * @property ActiveRecord $translation
 */
class BlogCategory extends ActiveRecord
{
    public $_form;

    public static function create($form, $slug, $sort): self
    {
        $category        = new static();
        $category->slug  = $slug;
        $category->sort  = $sort;
        $category->_form = $form;

        return $category;
    }

    public function edit($form, $slug, $sort): void
    {
        $this->slug  = $slug;
        $this->sort  = $sort;
        $this->_form = $form;
    }

    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('blog', 'ID'),
            'name'             => Yii::t('blog', 'Name'),
            'slug'             => Yii::t('blog', 'SEO link'),
            'sort'             => Yii::t('blog', 'Sort'),
            'title'            => Yii::t('blog', 'Title'),
            'description'      => Yii::t('blog', 'Description'),
            'meta_title'       => Yii::t('blog', 'META Title'),
            'meta_description' => Yii::t('blog', 'META Description'),
            'meta_keywords'    => Yii::t('blog', 'META Keywords'),
        ];
    }

    public function getSeoTitle(): string
    {
        return $this->translation->meta_title ?: $this->getHeadingTile();
    }

    public function getHeadingTile(): string
    {
        return $this->translation->title ?: $this->translation->name;
    }

    public function getPostsCount(): int
    {
        return BlogPost::find()->where(['category_id' => $this->id])->count('*');
    }

    public static function tableName(): string
    {
        return '{{%blog_categories}}';
    }

    public function behaviors(): array
    {
        return [
            /*[
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
                'ensureUnique' => true,
                'preserveNonEmptyValues' => true,
            ],*/
            [
                'class'               => LanguageBehavior::class,
                'virtualClassName'    => 'BlogCategoryVirtualTranslate',
                'translatedLanguages' => \Yii::$app->params['translatedLanguages'],
                'relativeField'       => 'blog_category_id',
                'tableName'           => '{{%blog_categories_lng}}',
                'attributes'          => [
                    'title',
                    'description',
                    'name',
                    'meta_title',
                    'meta_description',
                    'meta_keywords'
                ],
                //'defaultLanguage' => basename(Yii::$app->getBasePath()) === 'backend' ? Yii::$app->language : Yii::$app->params['defaultLanguage'],
                'defaultLanguage'     => \Yii::$app->params['defaultLanguage'],
            ],
            [
                'class'     => SluggableRelationBehavior::class,
                'attribute' => 'name',
                'relation'  => 'translation',
            ],
        ];
    }

    public static function find(): BlogCategoryQuery
    {
        return new BlogCategoryQuery(static::class);
    }
}
