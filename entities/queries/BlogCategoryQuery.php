<?php
/**
 * Created by Error202
 * Date: 27.07.2018
 */

namespace common\modules\blog\entities\queries;

use yii\db\ActiveQuery;
use core\components\LanguageTranslateTrait;

class BlogCategoryQuery extends ActiveQuery
{
    use LanguageTranslateTrait;
}
