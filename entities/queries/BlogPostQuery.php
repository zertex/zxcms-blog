<?php

namespace common\modules\blog\entities\queries;

use common\modules\blog\entities\BlogPost;
use core\components\LanguageTranslateTrait;
use yii\db\ActiveQuery;

class BlogPostQuery extends ActiveQuery
{
    use LanguageTranslateTrait;

    /**
     * @param null $alias
     *
     * @return $this
     */
    public function active($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'status' => BlogPost::STATUS_ACTIVE,
        ]);
    }

    public function pubDate()
    {
        return $this->andWhere(['<', 'published_at', time()]);
    }

    public function typePublic($alias = null)
    {
        return $this->andWhere([
            ($alias ? $alias . '.' : '') . 'type' => BlogPost::TYPE_PUBLIC,
        ]);
    }

    public function last()
    {
        return $this->orderBy(['published_at' => SORT_DESC]);
    }

    public function popular()
    {
        return $this->orderBy(['views' => SORT_DESC]);
    }
}
