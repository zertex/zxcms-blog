<?php

namespace common\modules\blog\entities;

use core\entities\user\User;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use Yii;

/**
 * @property int $id
 * @property int $created_at
 * @property int $post_id
 * @property int $user_id
 * @property int $parent_id
 * @property string $text
 * @property bool $active
 *
 * @property BlogPost $post
 */
class BlogComment extends ActiveRecord
{
    public static function create($userId, $parentId, $text): self
    {
        $review = new static();
        $review->user_id = $userId;
        $review->parent_id = $parentId;
        $review->text = $text;
        $review->created_at = time();
        $review->active = true;
        return $review;
    }

    public function edit($parentId, $text): void
    {
        $this->parent_id = $parentId;
        $this->text = $text;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('blog', 'ID'),
            'user_id' => Yii::t('blog', 'User'),
            'parent_id' => Yii::t('blog', 'Parent Comment ID'),
            'created_at' => Yii::t('blog', 'Created At'),
            'active' => Yii::t('blog', 'Published'),
            'post_id' => Yii::t('blog', 'Post'),
            'text' => Yii::t('blog', 'Comment'),
        ];
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function draft(): void
    {
        $this->active = false;
    }

    public function isActive(): bool
    {
        return $this->active == true;
    }

    public function isIdEqualTo($id): bool
    {
        return $this->id == $id;
    }

    public function isChildOf($id): bool
    {
        return $this->parent_id == $id;
    }

    public function getPost(): ActiveQuery
    {
        return $this->hasOne(BlogPost::class, ['id' => 'post_id']);
    }

    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function tableName(): string
    {
        return '{{%blog_comments}}';
    }
}
