<?php

namespace common\modules\blog\entities;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * @property integer $post_id;
 * @property integer $tag_id;
 *
 * @property BlogTag $tag
 */
class BlogTagAssignment extends ActiveRecord
{
    public static function create($tagId): self
    {
        $assignment         = new static();
        $assignment->tag_id = $tagId;

        return $assignment;
    }

    public function isForTag($id): bool
    {
        return $this->tag_id == $id;
    }

    public static function tableName(): string
    {
        return '{{%blog_tag_assignments}}';
    }

    public function getTag(): ActiveQuery
    {
        return $this->hasOne(BlogTag::class, ['id' => 'tag_id']);
    }
}
