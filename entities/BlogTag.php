<?php

namespace common\modules\blog\entities;

use yii\db\ActiveRecord;
use yii\caching\TagDependency;
use yii\behaviors\SluggableBehavior;
use Yii;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 */
class BlogTag extends ActiveRecord
{
    public static function create($name, $slug): self
    {
        $tag       = new static();
        $tag->name = $name;
        $tag->slug = $slug;

        return $tag;
    }

    public function edit($name, $slug): void
    {
        $this->name = $name;
        if ($slug != $this->slug) {
            TagDependency::invalidate(\Yii::$app->cache, 'blog_tags');
        }
        $this->slug = $slug;
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('blog', 'Tag Name'),
            'slug' => Yii::t('blog', 'SEO link'),
        ];
    }

    public static function tableName(): string
    {
        return '{{%blog_tags}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'                  => SluggableBehavior::class,
                'attribute'              => 'name',
                'ensureUnique'           => true,
                'preserveNonEmptyValues' => true,
            ],
        ];
    }
}
