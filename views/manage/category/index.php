<?php

use common\modules\blog\entities\BlogCategory;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\blog\forms\search\BlogCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title = Yii::t('blog', 'Categories');
$this->title = $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-category-index">

    <p>
        <?= Html::a(Yii::t('blog', 'Create Category'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'sort',
                        'options' => ['style' => 'width: 100px;'],
                    ],
                    [
                        'attribute' => 'name',
                        'value' => function (BlogCategory $model) {
                            return Html::a(Html::encode($model->translation->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'slug',
                    ],
	                [
		                'attribute' => 'title',
		                'value' => function (BlogCategory $model) {
			                $title = isset($model->findTranslation(Yii::$app->language)->title) ? $model->findTranslation(Yii::$app->language)->title : $model->findTranslation(Yii::$app->params['defaultLanguage'])->title;
			                return Html::a(Html::encode($title), ['view', 'id' => $model->id]);
		                },
		                'format' => 'raw',
	                ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
