<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $category \common\modules\blog\entities\BlogCategory */

$title                         = $category->translation->name;
$this->title                   = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$css = <<<CSS
	.detail-view th {
		width: 25%;
	}
CSS;
$this->registerCss($css);
?>

<div class="blog-category-view">

    <p>
        <?= Html::a(Yii::t('blog', 'Categories'), ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $category->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $category->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <div class="box">
        <div class="box-header with-border"><?= Yii::t('blog', 'Common') ?></div>
        <div class="box-body">
            <?= DetailView::widget([
                'model'      => $category,
                'attributes' => [
                    'id',
                    'slug',
                    'sort',
                ],
            ]) ?>
        </div>
    </div>

    <?php
    $items = [];
    foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
        $items[] = [
            'label'   => $language_name,
            'content' => $this->render('_view_tab', [
                'category' => $category,
                'language' => $language,
            ]),
        ];
    }
    ?>

    <div class="nav-tabs-custom">
        <?= \yii\bootstrap\Tabs::widget([
            'items' => $items,
        ]) ?>
    </div>

</div>
