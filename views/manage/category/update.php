<?php

/* @var $this yii\web\View */
/* @var $category \common\modules\blog\entities\BlogCategory */
/* @var $model \common\modules\blog\forms\BlogCategoryForm */

$title = Yii::t('blog', 'Update: {name}', ['name' => $category->translation->name]);
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $category->translation->name, 'url' => ['view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
