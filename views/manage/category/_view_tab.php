<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\blog\entities\BlogCategory;

/**
 * @var $this \yii\web\View
 * @var $category BlogCategory
 * @var $language string
 */

?>

<?= DetailView::widget([
	'model' => $category,
	'attributes' => [
		[
			'label' => Yii::t('blog', 'Name'),
			'value' => function(BlogCategory $entity) use ($language) {
				return $entity->findTranslation($language)->name;
			}
		],
		[
			'label' => Yii::t('blog', 'Title'),
			'value' => function(BlogCategory $entity) use ($language) {
				return $entity->findTranslation($language)->title;
			}
		],
	],
]) ?>

<div class="box">
	<div class="box-header with-border"><?= Yii::t('blog', 'SEO') ?></div>
	<div class="box-body">

<?= DetailView::widget([
	'model' => $category,
	'attributes' => [
		[
			'label' => Yii::t('blog', 'META Title'),
			'value' => function(BlogCategory $entity) use ($language) {
				return $entity->findTranslation($language)->meta_title;
			}
		],
		[
			'label' => Yii::t('blog', 'META Description'),
			'value' => function(BlogCategory $entity) use ($language) {
				return $entity->findTranslation($language)->meta_description;
			}
		],
		[
			'label' => Yii::t('blog', 'META Keywords'),
			'value' => function(BlogCategory $entity) use ($language) {
				return $entity->findTranslation($language)->meta_keywords;
			}
		],
	],
]) ?>
	</div>
</div>

<div class="box">
	<div class="box-header with-border"><?= Yii::t('blog', 'Description') ?></div>
	<div class="box-body">
		<?= Yii::$app->formatter->asHtml($category->findTranslation($language)->description, [
			'Attr.AllowedRel' => array('nofollow'),
			'HTML.SafeObject' => true,
			'Output.FlashCompat' => true,
			'HTML.SafeIframe' => true,
			'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
		]) ?>
	</div>
</div>