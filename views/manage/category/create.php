<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\forms\BlogCategoryForm */

$title = Yii::t('blog', 'Create Category');
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
