<?php

/* @var $this yii\web\View */
/* @var $post \common\modules\blog\entities\BlogPost */
/* @var $model \common\modules\blog\forms\BlogPostForm */

use yii\helpers\StringHelper;

$title = Yii::t('blog', 'Update Post: {name}', ['name' => StringHelper::truncateWords($post->translation->title, 6, '...')]);
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncateWords($post->translation->title, 4, '...'), 'url' => ['view', 'id' => $post->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="post-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
