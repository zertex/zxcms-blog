<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use common\modules\blog\helpers\BlogPostHelper;

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\forms\BlogPostForm */
/* @var $form yii\widgets\ActiveForm */

$fetchUrl = Url::to(['tag-search']);

if (isset($model->_post)) {
    $tagsJson = Json::encode(Json::encode(ArrayHelper::map($model->_post->tags, 'id', 'name')));

    $js = <<<JS
	var arr = {$tagsJson};
	$.each(JSON.parse(arr), function( key, value ) {
  		$("#posttagform-new_tags").append("<option value='"+value+"' selected>"+value+"</option>");
	});
	$('#posttagform-new_tags').trigger('change');
JS;
    $this->registerJs($js, $this::POS_READY);
}

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>


    <div class="row">
        <div class="col-md-10">


            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-body">
                            <?= $form->field($model, 'category_id')->dropDownList($model->categoriesList()) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-default">
                        <div class="box-body">
                            <?= $form->field($model->tags, 'new_tags')->widget(Select2::class, [
                                'options'       => [
                                    'placeholder' => Yii::t('blog', 'Set tags...'),
                                    'multiple'    => true,
                                ],
                                'pluginOptions' => [
                                    'tags'               => true,
                                    'tokenSeparators'    => [',', ' '],
                                    'maximumInputLength' => 12,
                                    'ajax'               => [
                                        'url'      => $fetchUrl,
                                        'dataType' => 'json',
                                        'data'     => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult'     => new JsExpression('function(tag) { return tag.text; }'),
                                    'templateSelection'  => new JsExpression('function (tag) { return tag.text; }'),
                                ],
                            ])->label(Yii::t('blog', 'Tags')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box box-default">
                <div class="box-body">
                    <!-- < ?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?> -->
                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                    <!-- < ?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
                    < ?= $form->field($model, 'content')->widget(CKEditor::class) ?> -->
                </div>
            </div>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_form_tab', [
                        'form'     => $form,
                        'model'    => $model,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="nav-tabs-custom">
                <?= \yii\bootstrap\Tabs::widget([
                    'items' => $items
                ]) ?>
            </div>

            <div class="box box-default">
                <div class="box-body">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#field_image"><?= Yii::t('blog', 'Image') ?></a>
                        </li>
                        <li><a data-toggle="tab" href="#field_video"><?= Yii::t('blog', 'Video') ?></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="field_image" class="tab-pane fade in active" style="padding-top: 20px;">
                            <?= $form->field($model, 'image')->label(false)->widget(FileInput::class, [
                                'options'       => [
                                    'accept' => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'showUpload' => false,
                                ],
                            ]) ?>

                            <?php if (isset($model->_post) && $model->_post->image) : ?>
                                <?= Html::img($model->_post->getThumbFileUrl('image', 'thumb_gallery_view'), [
                                    'class' => 'thumbnail',
                                    'width' => 300,
                                ]) ?>

                                <?= $form->field($model, 'reset_image')->checkbox() ?>
                            <?php endif; ?>
                        </div>
                        <div id="field_video" class="tab-pane fade" style="padding-top: 20px;">
                            <?= $form->field($model, 'video')->textInput()->label(Yii::t('blog', 'Video URL'))->hint(Yii::t('blog', 'If a video link is specified, the image will be used as a preview image')) ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('buttons', 'Save'), [
                    'class'      => 'btn btn-success',
                    'value'      => 'save',
                    'name'       => 'submit_save',
                    'formaction' => Yii::$app->request->getUrl(),
                    'formtarget' => '_self',
                ]) ?>
            </div>


        </div>
        <div class="col-md-2">
            <div class="box box-default">
                <div class="box-header with-border"><?= Yii::t('blog', 'Publish') ?></div>
                <div class="box-body">

                    <div class="btn-group">
                        <button type="button" class="btn btn-info"><?= Yii::t('blog', 'Preview on site') ?></button>
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <?php foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) : ?>
                                <li>
                                    <?= Html::submitButton($language_name, [
                                        'class'      => 'btn btn-block btn-flat bg-white',
                                        'value'      => 'preview',
                                        'name'       => 'submit_preview',
                                        'formaction' => \yii\helpers\Url::to([
                                            '/blog/manage/post/create-preview',
                                            'language' => $language == Yii::$app->params['defaultLanguage'] ? '' : $language
                                        ]),
                                        'formtarget' => '_blank',
                                        'style'      => 'border:0; background-color:#ffffff;',
                                    ]) ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>


                    <hr>
                    <?= $form->field($model, 'status')->radioList(BlogPostHelper::statusList()) ?>
                    <hr>
                    <?= $form->field($model, 'published_at')->widget(DateTimePicker::class, [
                        'options'       => [],
                        'removeButton'  => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format'    => 'dd.mm.yyyy hh:ii:ss',
                        ]
                    ])->label(Yii::t('blog', 'Publish Date')); ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
