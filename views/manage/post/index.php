<?php

use common\modules\blog\entities\BlogPost;
use common\modules\blog\helpers\BlogPostHelper;
use yii\helpers\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\blog\forms\search\BlogPostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title                         = Yii::t('blog', 'All Posts');
$this->title                   = $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-post-index">

    <p>
        <?= Html::a(Yii::t('blog', 'Create Post'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    [
                        'attribute'      => 'id',
                        'options'        => ['style' => 'width: 40px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'title',
                        'value'     => function (BlogPost $model) {
                            $title = isset($model->translation->title) ? $model->findTranslation(Yii::$app->language)->title : $model->findTranslation(Yii::$app->params['defaultLanguage'])->title;

                            return Html::a(Html::encode($title), ['view', 'id' => $model->id]);
                        },
                        'format'    => 'raw',
                    ],
                    [
                        'attribute' => 'category_id',
                        'filter'    => $searchModel->categoriesList(),
                        'value'     => function (BlogPost $model) {
                            return $model->category->translation->name;
                        },
                    ],
                    [
                        'attribute'      => 'published_at',
                        'format'         => ['datetime', 'php:d.m.Y H:i'],
                        'options'        => ['style' => 'width: 60px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute'      => 'status',
                        'filter'         => $searchModel->statusList(),
                        'value'          => function (BlogPost $model) {
                            return BlogPostHelper::statusLabel($model->status);
                        },
                        'format'         => 'raw',
                        'options'        => ['style' => 'width: 120px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'class'          => ActionColumn::class,
                        /*'urlCreator' => function($action, BlogPost $model, $key, $index, $column) {
                            $params = is_array($key) ? $key : ['id' => (string) $key];
                            $params[0] = $column->controller ? $column->controller . '/' . $action : $action;
                            return Url::toRoute($params);
                        },*/
                        'options'        => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
