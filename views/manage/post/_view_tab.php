<?php
/**
 * Created by Error202
 * Date: 25.08.2018
 */

use yii\widgets\DetailView;
use common\modules\blog\entities\BlogPost;

/**
 * @var $this \yii\web\View
 * @var $post \common\modules\blog\entities\BlogPost
 * @var $language string
 */

?>

<?= DetailView::widget([
	'model' => $post,
	'attributes' => [
		[
			'label' => Yii::t('blog', 'Title'),
			'value' => function(BlogPost $entity) use ($language) {
				return $entity->findTranslation($language)->title;
			}
		],
	],
]) ?>

<div class="box">
	<div class="box-header with-border"><?= Yii::t('blog', 'SEO') ?></div>
	<div class="box-body">

<?= DetailView::widget([
	'model' => $post,
	'attributes' => [
		[
			'label' => Yii::t('blog', 'META Title'),
			'value' => function(BlogPost $entity) use ($language) {
				return $entity->findTranslation($language)->meta_title;
			}
		],
		[
			'label' => Yii::t('blog', 'META Description'),
			'value' => function(BlogPost $entity) use ($language) {
				return $entity->findTranslation($language)->meta_description;
			}
		],
		[
			'label' => Yii::t('blog', 'META Keywords'),
			'value' => function(BlogPost $entity) use ($language) {
				return $entity->findTranslation($language)->meta_keywords;
			}
		],
	],
]) ?>
	</div>
</div>

<div class="box">
	<div class="box-header with-border"><?= Yii::t('blog', 'Description') ?></div>
	<div class="box-body">
		<?= Yii::$app->formatter->asNtext($post->findTranslation($language)->description) ?>
	</div>
</div>

<div class="box">
	<div class="box-header with-border"><?= Yii::t('blog', 'Content') ?></div>
	<div class="box-body">
		<?= Yii::$app->formatter->asHtml($post->findTranslation($language)->content, [
			'Attr.AllowedRel' => array('nofollow'),
			'HTML.SafeObject' => true,
			'Output.FlashCompat' => true,
			'HTML.SafeIframe' => true,
			'URI.SafeIframeRegexp'=>'%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%',
		]) ?>
	</div>
</div>