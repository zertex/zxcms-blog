<?php

use common\modules\blog\helpers\BlogPostHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $post \common\modules\blog\entities\BlogPost */
/* @var $modificationsProvider yii\data\ActiveDataProvider */
/* @var $history \common\modules\blog\entities\BlogPost[] */

$title                         = $post->translation->title;
$this->title                   = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$css = <<<CSS
.detail-view th {
	width: 25%;
}
CSS;
$this->registerCss($css);
?>
<div class="post-view">

    <p>
        <?= Html::a(Yii::t('blog', 'All Posts'), ['index'], ['class' => 'btn btn-default']) ?>
        <?php if ($post->isActive()) : ?>
            <?= Html::a(Yii::t('blog', 'Draft'), ['draft', 'id' => $post->id], ['class' => 'btn btn-primary', 'data-method' => 'post']) ?>
        <?php else : ?>
            <?= Html::a(Yii::t('blog', 'Activate'), ['activate', 'id' => $post->id], ['class' => 'btn btn-success', 'data-method' => 'post']) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('buttons', 'Edit'), ['update', 'id' => $post->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('buttons', 'Delete'), ['delete', 'id' => $post->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('buttons', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>


    <div class="row">
        <div class="col-md-9">

            <div class="box">
                <div class="box-header with-border"><?= Yii::t('blog', 'Common') ?></div>
                <div class="box-body">
                    <?= DetailView::widget([
                        'model'      => $post,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'status',
                                'value'     => BlogPostHelper::statusLabel($post->status),
                                'format'    => 'raw',
                            ],
                            [
                                'attribute' => 'category_id',
                                'value'     => $post->category->translation->name,
                                //ArrayHelper::getValue($post, 'category.name'),
                            ],
                            [
                                'label'  => Yii::t('post', 'Tags'),
                                'format' => 'raw',
                                'value'  => '<i class="fa fa-tag" aria-hidden="true"></i> ' . implode(' <i class="fa fa-tag" aria-hidden="true"></i> ', ArrayHelper::getColumn($post->tags, 'name')),
                            ],
                            'slug'
                        ],
                    ]) ?>
                </div>
            </div>

            <?php
            $items = [];
            foreach (Yii::$app->params['translatedLanguages'] as $language => $language_name) {
                $items[] = [
                    'label'   => $language_name,
                    'content' => $this->render('_view_tab', [
                        'post'     => $post,
                        'language' => $language,
                    ]),
                ];
            }
            ?>

            <div class="nav-tabs-custom">
                <?= \yii\bootstrap\Tabs::widget([
                    'items' => $items,
                ]) ?>
            </div>

            <?php if ($post->image) : ?>
                <div class="box">
                    <div class="box-header with-border"><?= Yii::t('blog', 'Image') ?></div>
                    <div class="box-body">
                        <?= Html::img($post->getThumbFileUrl('image', 'thumb_gallery_view'), [
                            'class' => 'thumbnail',
                            'width' => 300,
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (!$post->image && $post->video) : ?>
                <div class="box">
                    <div class="box-header with-border"><?= Yii::t('blog', 'Image') ?></div>
                    <div class="box-body">
                        <!-- < ?= Html::img('https://i.ytimg.com/vi/' . BlogPostHelper::parseYoutubeUrl($post->video) . '/maxresdefault.jpg', [
                            'width' => 300,
                            'class' => 'thumbnail',
                        ]) ?> -->
                        <?= Html::img(\core\helpers\VideoHelper::getThumb($post->video), [
                            'width' => 300,
                            'class' => 'thumbnail',
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($post->video) : ?>
                <div class="box">
                    <div class="box-header with-border"><?= Yii::t('blog', 'Video') ?></div>
                    <div class="box-body">
                        <?= \core\widgets\VideoWidget::widget([
                            'src'   => $post->video,
                            'style' => '',
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-md-3">
            <div class="box">
                <div class="box-header with-border"><?= Yii::t('blog', 'History') ?></div>
                <div class="box-body">
                    <?php if ($history) : ?>
                        <ul>
                            <?php foreach ($history as $item) : ?>
                                <li>
                                    <?php if ($item->revision_id) : ?>
                                        <?= date('d.m.Y H:i', $item->revision_at) ?>
                                        →
                                        <?= Html::a(Yii::t('blog', 'View'),
                                            \yii\helpers\Url::to(Yii::$app->get('frontendUrlManager')->createAbsoluteUrl([
                                                '/blog/post/preview',
                                                'id' => $item->id
                                            ])),
                                            [
                                                'style'  => 'font-size:11px;',
                                                'target' => '_blank',
                                            ]
                                        ) ?>
                                        |

                                        <?= Html::a(Yii::t('blog', 'Restore'), [
                                                '/blog/manage/post/restore-history',
                                                'id' => $item->id
                                        ], [
                                                'style' => 'font-size:11px; color: red',
                                                'data'  => [
                                                    'confirm' => Yii::t('blog', 'Are you sure you want to restore this history item?'),
                                                    'method'  => 'post',
                                                ],
                                            ]) ?>

                                    <?php else : ?>
                                        <strong><?= Yii::t('blog', 'Current Edition') ?></strong>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?= Html::a(Yii::t('blog', 'Clear History'), [
                                '/blog/manage/post/clear-history',
                                'id' => $post->id
                            ], [
                                'class' => 'btn btn-danger btn-sm pull-right',
                                'data'  => [
                                    'confirm' => Yii::t('blog', 'Are you sure you want to remove this history?'),
                                    'method'  => 'post',
                                ],
                            ]) ?>
                    <?php else : ?>
                        <div style="padding: 20px 0; text-align: center"><?= Yii::t('blog', 'History is empty') ?></div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>


</div>