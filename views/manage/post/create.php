<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\forms\BlogPostForm */

$title = Yii::t('blog', 'Create Post');
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;
?>
<div class="post-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
