<?php

/* @var $this yii\web\View */
/* @var $tag \common\modules\blog\entities\BlogTag */
/* @var $model \common\modules\blog\forms\BlogTagSingleForm */

$title = Yii::t('blog', 'Update Tag: {name}', ['name' => $tag->name]);
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $tag->name, 'url' => ['view', 'id' => $tag->id]];
$this->params['breadcrumbs'][] = Yii::t('buttons', 'Editing');
?>
<div class="tag-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
