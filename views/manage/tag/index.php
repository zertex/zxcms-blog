<?php

use common\modules\blog\entities\BlogTag;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\blog\forms\search\BlogTagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$title = Yii::t('blog', 'Tags');
$this->title = $title;
$this->params['breadcrumbs'][] = $title;
?>
<div class="blog-tags-index">

    <p>
        <?= Html::a(Yii::t('blog', 'Create Tag'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'name',
                        'value' => function (BlogTag $model) {
                            return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'slug',
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
