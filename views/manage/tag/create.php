<?php

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\forms\BlogTagSingleForm */

$title = Yii::t('blog', 'Create Tag');
$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
