<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\blog\forms\BlogTagSingleForm */
/* @var $form yii\widgets\ActiveForm */

$js2 = '
$(".hint-block").each(function () {
    var $hint = $(this);
    var label = $hint.parent().find("label");
    label.html(label.html() + \' <i style="color:#3c8dbc" class="fa fa-question-circle" aria-hidden="true"></i>\');
    label.addClass("help").popover({
        html: true,
        trigger: "hover",
        placement: "bottom",
        content: $hint.html()
    });
    $(this).hide();
});
';
$this->registerJs($js2);
?>

<div class="tag-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-default">
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('buttons', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
