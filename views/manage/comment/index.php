<?php

use common\modules\blog\entities\BlogComment;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\blog\forms\search\BlogCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('blog', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blog-comments-index">
    <div class="box">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'options' => ['style' => 'width: 40px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                    [
                        'attribute' => 'text',
                        'value' => function (BlogComment $model) {
                            return StringHelper::truncate(strip_tags($model->text), 100);
                        },
                    ],
	                [
		                'attribute' => 'created_at',
		                'format' => ['datetime', 'php:d.m.Y H:i'],
		                'options' => ['style' => 'width: 60px;'],
		                'contentOptions' => ['class' => 'text-center'],
	                ],
                    [
                        'attribute' => 'active',
                        'filter' => $searchModel->activeList(),
                        'format' => 'boolean',
                        'contentOptions' => ['style' => 'width: 150px'],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'options' => ['style' => 'width: 100px;'],
                        'contentOptions' => ['class' => 'text-center'],
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
