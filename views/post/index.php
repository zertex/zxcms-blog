<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */


$this->title = Yii::t('blog', 'Blog');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>