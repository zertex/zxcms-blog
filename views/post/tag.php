<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\DataProviderInterface */
/* @var $tag \common\modules\blog\entities\BlogTag */

use yii\helpers\Html;

$this->title = $tag->name;

$this->registerMetaTag(['name' =>'description', 'content' => '']);
$this->registerMetaTag(['name' =>'keywords', 'content' => '']);

$this->params['breadcrumbs'][] = ['label' => Yii::t('blog', 'Blog'), 'url' => ['/blog/post/index']];
$this->params['breadcrumbs'][] = $tag->name;
?>

<h1 class="my-4"><?= Html::encode($tag->name) ?></h1>

<?= $this->render('_list', [
    'dataProvider' => $dataProvider
]) ?>


